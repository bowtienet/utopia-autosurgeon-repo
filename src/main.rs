use std::net::Ipv6Addr;
use cidr::Ipv6Cidr;
use autosurgeon::{Reconcile, Hydrate};
use utoipa::ToSchema;



#[derive(Debug, Clone, Reconcile, Hydrate, PartialEq)]
struct ExamplePeerSurgeon {
    id: u64,
    #[autosurgeon(with = "ipv6cidr_impls")]
    ip_range: Ipv6Cidr,
}

#[derive(ToSchema, Debug)]
struct ExamplePeerUtoipa {
    id: u64,
    #[schema(value_type=String)]
    ip_range: Ipv6Cidr,
}

#[derive(Debug, Clone, Reconcile, Hydrate, PartialEq, ToSchema)]
struct ExamplePeerBoth {
    id: u64,
    /* Uncommenting this code block resutls in "error: expected literal" on the #[schema] line.
        #[schema(value_type=String)]
    */
    #[autosurgeon(with = "ipv6cidr_impls")]
    ip_range: Ipv6Cidr,
}


mod ipv6cidr_impls {
    // STuck again on this only working while I'm in the related module :(
    use autosurgeon::{hydrate::Unexpected, Hydrate, HydrateError, Prop, ReadDoc, Reconciler};
    use cidr::Ipv6Cidr;
    use std::str::FromStr;

    pub fn hydrate<D: ReadDoc>(
        doc: &D,
        obj: &automerge::ObjId,
        prop: Prop<'_>,
    ) -> Result<Ipv6Cidr, HydrateError> {
        let inner = String::hydrate(doc, obj, prop)?;
        let inner = Ipv6Cidr::from_str(&inner).map_err(|e| {
            HydrateError::Unexpected(Unexpected::Other {
                expected: "an IPv6 CIDR range".into(),
                found: e.to_string(),
            })
        })?;
        Ok(inner)
    }

    pub fn reconcile<R: Reconciler>(a_cidr: &Ipv6Cidr, mut reconciler: R) -> Result<(), R::Error> {
        reconciler.str(a_cidr.to_string())
    }

    pub mod option {
        use autosurgeon::{hydrate::Unexpected, Hydrate, HydrateError, Prop, ReadDoc, Reconciler};
        use cidr::Ipv6Cidr;
        use std::str::FromStr;

        pub fn hydrate<D: ReadDoc>(
            doc: &D,
            obj: &automerge::ObjId,
            prop: Prop<'_>,
        ) -> Result<Option<Ipv6Cidr>, HydrateError> {
            let inner = Option::<String>::hydrate(doc, obj, prop)?;
            let inner = inner
                .map(|inner| {
                    Ipv6Cidr::from_str(&inner).map_err(|e| {
                        HydrateError::Unexpected(Unexpected::Other {
                            expected: "an IPv6 CIDR range or null".into(),
                            found: e.to_string(),
                        })
                    })
                })
                .transpose()?;
            Ok(inner)
        }

        pub fn reconcile<R: Reconciler>(
            a_cidr: &Option<Ipv6Cidr>,
            mut reconciler: R,
        ) -> Result<(), R::Error> {
            match a_cidr {
                Some(a_cidr) => reconciler.str(a_cidr.to_string()),
                None => reconciler.none(),
            }
        }
    }

}


mod example_api {
    // Goal is to use both Autosurgeon and Utoipa
    //use crate::ExamplePeerBoth;

    // Actually can only use Utoipa
    use crate::ExamplePeerUtoipa;
    use std::net::Ipv6Addr;
    use cidr::Ipv6Cidr;

    /// Get pet by id
    ///
    /// Get pet from database by pet id
    /*
    #[utoipa::path(
        get,
        path = "/example/{id}",
        responses(
            (status = 200, description = "Pet found successfully", body = Pet),
            (status = NOT_FOUND, description = "Pet was not found")
        ),
        params(
            ("id" = u64, Path, description = "Pet database id to get Pet for"),
        )
    )]
    fn get_peer_by_id(id: u64) -> ExamplePeerBoth {
        ExamplePeerBoth{
            id: id,
            ip_range: Ipv6Cidr::new(
                Ipv6Addr::new(0,0,0,0,0,0,0,1), 128
            ).unwrap()
        }
    }
    */

    #[utoipa::path(
        get,
        path = "/example/{id}",
        responses(
            (status = 200, description = "Pet found successfully", body = Pet),
            (status = NOT_FOUND, description = "Pet was not found")
        ),
        params(
            ("id" = u64, Path, description = "Pet database id to get Pet for"),
        )
    )]
    fn get_peer_by_id(id: u64) -> ExamplePeerUtoipa {
        ExamplePeerUtoipa{
            id: id,
            ip_range: Ipv6Cidr::new(
                Ipv6Addr::new(0,0,0,0,0,0,0,1), 128
            ).unwrap()
        }
    }

}

fn main() {
    println!("Hello, world!");

    let e_surgeon = ExamplePeerSurgeon{
        id: 1, 
        ip_range: Ipv6Cidr::new(
            Ipv6Addr::new(0,0,0,0,0,0,0,1), 128
        ).unwrap()
    };
    let e_utoipa = ExamplePeerUtoipa{
        id: 1, 
        ip_range: Ipv6Cidr::new(
            Ipv6Addr::new(0,0,0,0,0,0,0,1), 128
        ).unwrap()
    };

    println!("Good So Far {:?} {:?}", e_surgeon, e_utoipa);
    
    let e_both = ExamplePeerBoth{
        id: 1, 
        ip_range: Ipv6Cidr::new(
            Ipv6Addr::new(0,0,0,0,0,0,0,1), 128
        ).unwrap()
    };
    println!("Does this work? {:?}", e_both);

    use utoipa::OpenApi;
    #[derive(OpenApi)]
    #[openapi(paths(example_api::get_peer_by_id), components(schemas(ExamplePeerUtoipa)))]
    struct ApiDoc;

    println!("{}", ApiDoc::openapi().to_pretty_json().unwrap());

}
